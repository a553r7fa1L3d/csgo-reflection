DT_CascadeLight, base DT_BaseEntity
8 properties

DataTable baseclass ("DT_BaseEntity")
Vector m_shadowDirection
Vector m_envLightShadowDirection
Int32 m_bEnabled
Int32 m_bUseLightEnvAngles
Int32 m_LightColor
Int32 m_LightColorScale
Single m_flMaxShadowDist
