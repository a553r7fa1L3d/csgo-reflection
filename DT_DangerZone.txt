DT_DangerZone, base DT_BaseEntity
8 properties

DataTable baseclass ("DT_BaseEntity")
Vector m_vecDangerZoneOriginStartedAt
Single m_flBombLaunchTime
Single m_flExtraRadius
Single m_flExtraRadiusStartTime
Single m_flExtraRadiusTotalLerpTime
Int32 m_nDropOrder
Int32 m_iWave
