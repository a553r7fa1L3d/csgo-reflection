DT_WorldVguiText, base DT_BaseEntity
7 properties

DataTable baseclass ("DT_BaseEntity")
Int32 m_bEnabled
String m_szDisplayText (length 512)
String m_szDisplayTextOption (length 256)
String m_szFont (length 64)
Int32 m_iTextPanelWidth
Int32 m_clrText
