DT_PlayerPing, base DT_BaseEntity
6 properties

DataTable baseclass ("DT_BaseEntity")
Int32 m_hPlayer
Int32 m_hPingedEntity
Int32 m_iType
Int32 m_bUrgent
String m_szPlaceName (length 18)
